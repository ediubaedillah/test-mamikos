<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    /**
     * @group skip
     */
    public function test_register()
    {

        $formData = [
            'name' => 'John Doe',
            'email' => 'johndoe@gmail.com',
            'phone' => '0812939009300',
            'password' => '12345678',
            'type'  => 1
        ];
        $response = $this->post('api/register', $formData);
        $response->assertStatus(201);
    }
}
