<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AskAvailableKostTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_askAvailableKost()
    {
        $user = User::whereType(1)->first();
        $formData = [
            'kost_id' => 1
        ];
        $response = $this->actingAs($user)->post('/api/kost/askAvailable', $formData);

        $response->assertStatus(200);
    }
}
