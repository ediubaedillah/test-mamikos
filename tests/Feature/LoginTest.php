<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_login()
    {
        $formData = [
            'phone' => '0812939009300',
            'password' => '12345678',
        ];
        $response = $this->post('api/login', $formData);

        $response->assertStatus(200);
    }
}
