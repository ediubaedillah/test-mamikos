<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateKostTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_createKost()
    {
        $formData = [
            'name' => 'Kos Haji Jaka',
            'location' => 'Cianjur',
            'total' => 10,
            'price' => 100000,
            'is_available' => 1
        ];
        $user = User::whereType(3)->first();
        $response = $this->actingAs($user)->post('/api/kost/create', $formData);

        $response->assertStatus(201);
    }
}
