<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegisterOwnerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    /**
     * @group skip
     */
    public function test_regiterOwner()
    {
        $formData = [
            'name' => 'Owner',
            'email' => 'owner@gmail.com',
            'phone' => '0812939009301',
            'password' => '12345678',
            'type'  => 3
        ];
        $response = $this->post('api/register', $formData);
        $response->assertStatus(201);
    }
}
