<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OwnerDashboardTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_ownerDashboard()
    {
        $user = User::whereType(3)->first();
        $response = $this->actingAs($user)->get('/api/user/owner/dashboard');

        $response->assertStatus(200);
    }
}
