<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegisterPremiumTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    /**
     * @group skip
     */
    public function test_registerPremium()
    {
        $formData = [
            'name' => 'Premium',
            'email' => 'premium@gmail.com',
            'phone' => '0812939009302',
            'password' => '12345678',
            'type'  => 2
        ];
        $response = $this->post('api/register', $formData);
        $response->assertStatus(201);
    }
}
