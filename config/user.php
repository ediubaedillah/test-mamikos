<?php
return [
    'type' => [
        1 => 'User Regular',
        2 => 'User Premium',
        3 => 'Owner'
    ],
    'point' => [
        1 => 20,
        2 => 40
    ],
    'type_key_name' => [
        'user_regular' => 1,
        'user_premium' => 2,
        'owner' => 3,
    ]
];
