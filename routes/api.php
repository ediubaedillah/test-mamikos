<?php

use App\Http\Controllers\api\KostController;
use App\Http\Controllers\api\UserController;
// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

$router->post('/register', [UserController::class, 'register']);
$router->post('/login', [UserController::class, 'login']);
$router->group([
    'prefix' => '/kost'
], function () use ($router) {
    $router->get('/', [KostController::class, 'list']);
    $router->post('/create', [KostController::class, 'create'])->middleware(['auth-api', 'role-owner']);
    $router->post('/askAvailable', [KostController::class, 'askAvailableRoom'])->middleware('auth-api');
});

$router->group([
    'prefix' => '/user'
], function () use ($router) {
    $router->get('/owner/dashboard', [UserController::class, 'ownerDashboard'])->middleware(['auth-api', 'role-owner']);
});
