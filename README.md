<h4>Langkah - langkah untuk menjalankan atau install projek:</h4><br>

1. buat database dengan nama testmamikos <br>
2. ubah file .env.example menjadi .env <br>
3. pada file .env untuk DB_USERNAME dan DB_PASSWORD sesuaikan dengan settingan local DB anda <br>
4. jalankan perintah <b>composer install</b> untuk melakukan instalasi package-package yang dibutuhkan <br>
5. jalankan perintah <b>php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"</b> untuk publish package JWT <br>
6. jalankan perintah <b>php artisan migrate</b> untuk create table yang dibutuhkan <br>
7. jalankan perintah <b>php artisan serve</b> untuk menjalankan aplikasi <br>

<br>
<br>
<br>
<h4>Langkah - langkah untuk menjalankan unit test:</h4><br>

1. jalankan perintah <b>php artisan test --filter RegisterTest</b> untuk melakukan register user regular<br>
2. jalankan perintah <b>php artisan test --filter RegisterPremiumTest</b> untuk melakukan register user premium<br>
3. jalankan perintah <b>php artisan test --filter RegisterOwnerTest</b> untuk melakukan register user owner<br>
4. jalan perintah <b>php artisan test --exclude skip</test> untuk menjalankan test yang lainnya

<br>
<br>
<br>
<h4>Daftar API / Fitur:</h4><br>

1. <b>/api/register</b> => API untuk register<br>
2. <b>/api/login</b> => API untuk login<br>
3. <b>/api/kost/</b> => API untuk menampilkan list kosan<br>
4. <b>/api/kost/askAvailable</b> => API untuk menanyakan ketersediaan room<br>
5. <b>/api/kost/create</b> => API untuk create kosan<br>
6. <b>/api/user/owner/dashboard</b> => API untuk dashboard owner<br>
7. Fitur untuk recharge point dapat dijalankan dengan perintah <b>php artisan recharge_point</b>

<p>
    Note: untuk API ask room, create kost dan ownere dashboard menggunakan authorization token tipe Bearer. Token didapat setelah login berhasil.
</p>
