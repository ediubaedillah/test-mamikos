<?php

namespace App\Repositories;

use App\Models\Kost;
use App\Models\RoomAvailable;
use Illuminate\Support\Facades\Log;

class KostRepository
{
    public function create($data)
    {
        try {
            return Kost::create($data);
        } catch (\Exception $e) {
            Log::debug("Exception: " . json_encode($e));
            return false;
        }
    }

    public function list($filter)
    {
        try {
            $result = Kost::where(function ($query) use ($filter) {
                $query
                    ->orWhere('name', 'like', '%' . $filter['search'] . '%')
                    ->orWhere('location', 'like', '%' . $filter['search'] . '%')
                    ->orWhere('price', 'like', '%' . $filter['search'] . '%');
            })
                ->where(function ($query) use ($filter) {
                    if (isset($filter['user_id'])) {
                        $query->where('user_id', $filter['user_id']);
                    }
                })
                ->orderBy('price', 'asc')
                ->paginate(env('LIMIT_PER_PAGE'));
        } catch (\Exception $e) {
            Log::debug("Exception: " . json_encode($e));
            return false;
        }
        return $result;
    }

    public function trx_room_availability($data)
    {
        try {
            $result = RoomAvailable::create($data);
        } catch (\Exception $e) {
            Log::debug("Exception: " . json_encode($e));
            return false;
        }
        return $result;
    }

    public function countKostAvailable($user_id)
    {
        try {
            $result = Kost::where('is_available', 1)->whereUserId($user_id)
                ->get()
                ->count();
        } catch (\Exception $e) {
            Log::debug("Exception: " . json_encode($e));
            return false;
        }
        return $result;
    }

    public function countAskAvailable($user_id)
    {
        try {
            $result = RoomAvailable::leftJoin('kosts', 'kosts.id', '=', 'room_availabilities.kost_id')
                ->where('kosts.user_id', $user_id)
                ->get()
                ->count();
        } catch (\Exception $e) {
            Log::debug("Exception: " . json_encode($e));
            return false;
        }
        return $result;
    }
}
