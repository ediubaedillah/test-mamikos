<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserRepository
{
    public function create($data)
    {
        try {
            return User::create($data);
        } catch (\Exception $e) {
            Log::debug("Exception: " . json_encode($e));
            return false;
        }
    }

    public function reducePoint($user_id, $point)
    {
        try {
            $user = User::whereId($user_id)->first();
            $point = $user->point - $point;
            return User::whereId($user_id)->update([
                'point' => $point
            ]);
        } catch (\Exception $e) {
            Log::debug("Exception: " . json_encode($e));
            return false;
        }
    }

    public function rechargePoint()
    {
        try {
            $result = User::whereIn('type', [1, 2])->update([
                'point' => DB::raw('if(type=1, 20, 40)')
            ]);
        } catch (\Exception $e) {
            Log::debug("Exception: " . json_encode($e));
            return false;
        }
        return $result;
    }
}
