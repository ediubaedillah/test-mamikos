<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    protected $userService = null;
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function register(Request $request)
    {
        return $this->userService->create($request->all());
    }

    public function login(Request $request)
    {
        return $this->userService->login($request->all());
    }

    public function ownerDashboard()
    {
        $user = Auth::user();
        $userService = new UserService;
        return $userService->ownerDashboard($user);
    }
}
