<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Services\KostService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KostController extends Controller
{
    protected $kostService = null;
    public function __construct(KostService $kostService)
    {
        $this->kostService = $kostService;
    }

    public function create(Request $request)
    {
        $user = Auth::user();
        return $this->kostService->create($request->all(), $user);
    }

    public function list(Request $request)
    {
        return $this->kostService->list($request->all());
    }

    public function askAvailableRoom(Request $request)
    {
        $user = Auth::user();
        $kost_id = $request->kost_id;
        return $this->kostService->isAvailable($user->id, $kost_id);
    }
}
