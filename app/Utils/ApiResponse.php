<?php

namespace App\Utils;

/**
 *
 */
trait ApiResponse
{
    protected function success($data = null, $message = null, $code = null)
    {
        return response([
            "code" => $code,
            "message" => $message,
            "data" => $data
        ], $code);
    }

    protected function errors($data = null, $message = null, $code = null)
    {
        return response([
            "code" => $code,
            "message" => $message,
            "data" => $data
        ], $code);
    }

    protected function UnauthorizedUser($message, $title = null)
    {
        $message    = $message ?? 'Unauthorized User';
        $title      = $title ?? 'failed';
        return response([
            "code"    => 401,
            "message"   => $message,
            "data"    => []
        ], 401);
    }
}
