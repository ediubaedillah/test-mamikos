<?php

namespace App\Services;

use App\Repositories\KostRepository;
use App\Utils\ApiResponse;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Validator;
use JWTAuth;

class UserService
{
    use ApiResponse;
    public function create($data)
    {
        /** Validation */
        $validator = Validator::make($data, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'phone' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->errors($validator->errors(), 'validation error', 400);
        }
        /** End Validation */

        $user = [
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => app('hash')->make($data['password']),
            'phone' => $data['phone'],
            'point' => 0,
            'type' => $data['type'],
        ];

        if ($data['type'] == config('user.type_key_name.user_regular')) $user['point'] = config('user.point.1');
        if ($data['type'] == config('user.type_key_name.user_premium')) $user['point'] = config('user.point.2');
        $userRepo = new UserRepository;
        $result = $userRepo->create($user);
        if ($result) {
            return $this->success([], 'success', 201);
        } else {
            return $this->errors([], 'error', 400);
        }
    }

    public function login($data)
    {
        /** Validation */
        $validator = Validator::make($data, [
            'phone' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->errors($validator->errors(), 'validation error', 400);
        }
        /** End Validation */

        $credentials = [
            "phone" => $data['phone'],
            "password" => $data['password']
        ];

        $token = JWTAuth::attempt($credentials);
        if (!$token) {
            return $this->errors([], 'Phone and password wrong!', 400);
        }
        return $this->success(['token' => $token], 'success', 200);
    }

    public function rechargePoint()
    {
        $userRepo = new UserRepository;
        $result = $userRepo->rechargePoint();
        if (!$result) return $this->errors([], 'failed', 400);
        return $this->success([], 'success', 200);
    }

    public function ownerDashboard($user)
    {
        if (!$user) {
            return $this->UnauthorizedUser('You don\'t have access!', 'failed');
        }
        $kostRepository = new KostRepository;

        $kostAvailable = $kostRepository->countKostAvailable($user->id);
        $askRoom = $kostRepository->countAskAvailable($user->id);
        $kosts = $kostRepository->list(['search' => '', 'user_id' => $user->id]);
        return $this->success([
            'kost_available' => $kostAvailable,
            'ask_room_available' => $askRoom,
            'kosts' => $kosts
        ], 'success', 200);
    }
}
