<?php

namespace App\Services;

use App\Repositories\KostRepository;
use App\Repositories\UserRepository;
use App\Utils\ApiResponse;
use Illuminate\Support\Facades\Validator;

class KostService
{
    use ApiResponse;
    public function create($data, $user)
    {
        /** Validation */
        $validator = Validator::make($data, [
            'name' => 'required',
            'location' => 'required',
            'price' => 'required',
            'total' => 'required',
            'is_available' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errors($validator->errors(), 'validation error', 400);
        }
        /** End Validation */
        $kost = [
            'name' => $data['name'],
            'location' => $data['location'],
            'price' => $data['price'],
            'is_available' => $data['is_available'],
            'total' => $data['total'],
            'user_id' => $user->id,
        ];

        $kostRepo = new KostRepository;
        $result = $kostRepo->create($kost);
        if ($result) {
            return $this->success([], 'success', 201);
        } else {
            return $this->errors([], 'error', 400);
        }
    }

    public function list($filter)
    {
        $kostRepo = new KostRepository;
        if (is_array($filter) && count($filter) <= 0) $filter['search'] = '';
        $result = $kostRepo->list($filter);
        if (!$result) return $this->errors([], 'failed', 400);
        return $this->success($result, 'success', 200);
    }

    public function isAvailable($user_id, $kost_id)
    {
        $userRepo = new UserRepository;
        $kostRepo = new KostRepository;

        $result = $kostRepo->trx_room_availability([
            'user_id' => $user_id,
            'kost_id' => $kost_id,
            'trx_date'  => date('Y-m-d')
        ]);
        if (!$result) return $this->errors([], 'failed', 400);
        $userRepo->reducePoint($user_id, 5);
        return $this->success([], 'success', 200);
    }
}
