<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoomAvailable extends Model
{
    use HasFactory;
    protected $table = 'room_availabilities';
    protected $fillable = [
        'user_id',
        'kost_id',
        'trx_date',
    ];
}
